#!/usr/bin/python
import os
import sys
import glob
import cv2
import dlib

#if len(sys.argv) != 2:
#    print(
#        "Give the path to the examples/faces directory as the argument to this "
#        "program. For example, if you are in the python_examples folder then "
#        "execute this program by running:\n"
#        "    ./train_object_detector.py ../examples/faces")
#    exit()
#faces_folder = sys.argv[1]

options = dlib.simple_object_detector_training_options()

options.add_left_right_image_flips = False

options.C = 5

options.num_threads = 8
options.be_verbose = True
#options.detection_window_size = 600


training_xml_path = os.path.join("training/training.xml")#("/home/katsuro/Desktop/miniProject/imageclassifier/dataset/s01/training/s1train.xml")
testing_xml_path = os.path.join("testing/testing.xml")#("/home/katsuro/Desktop/miniProject/imageclassifier/dataset/s01/testing/s1test.xml")

dlib.train_simple_object_detector(training_xml_path, "detector_hand.svm", options)

print("")  
print("Training accuracy: {}".format(
    dlib.test_simple_object_detector(training_xml_path, "detector_hand.svm")))

print("Testing accuracy: {}".format(
    dlib.test_simple_object_detector(testing_xml_path, "detector_hand.svm")))

detector = dlib.simple_object_detector("detector_hand.svm")

print("Showing detections on the images in the faces folder...")

for f in glob.glob(os.path.join("testing/*.jpg")):#("/home/katsuro/Desktop/miniProject/imageclassifier/dataset/s01/testing/*.jpg")):
    print("Processing file: {}".format(f))
    img = dlib.load_rgb_image(f)
    dets = detector(img)
    print("Number of faces detected: {}".format(len(dets)))
    print(dets)
    for k, d in enumerate(dets):
        print("Detection {}: Left: {} Top: {} Right: {} Bottom: {}".format(
            k, d.left(), d.top(), d.right(), d.bottom()))
        cv2.rectangle(img,(d.left(), d.top()),(d.right(), d.bottom()),(0,255,0),2)
    cv2.imshow('image',cv2.resize(img,(int(500),int(500))))
    cv2.waitKey(0)
