#!/usr/bin/python3
import sys
import glob
import numpy as np
import cv2
import dlib
from skimage import io
#import socket    


#c, addr = s.accept()

def distance_to_camera(knownWidth, focalLength, perWidth):
	# compute and return the distance from the maker to the camera
	#print("known width")
	#print(knownWidth)
	#print("focalLength")
	#print(focalLength)
	#print("perWidth")
	#print(perWidth)
	#print("____________")
    return int((knownWidth * focalLength) / perWidth)


cap = cv2.VideoCapture(1)

detector_s1 = dlib.simple_object_detector("detector_hand.svm")

win_det = dlib.image_window()
win_det.set_image(detector_s1)
win = dlib.image_window()

while(True):
    #c, addr = s.accept()
    #print('Got connection from {} '.format( addr))
    ret, frame = cap.read()
    frame = cv2.flip(frame, -1)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    img = frame
    focalLength = 433.3717498779297
    
    #print("frame {}".format(len(frame)))
    dets_s1 = detector_s1(img)
    #dets_s2 = detector_s2(img)

    win.clear_overlay()
    win.set_image(img)
    win.add_overlay(dets_s1)
    
    for k, d in enumerate(dets_s1):
        #if k!=0:
            print("Left: {} Right: {} Left-Right: {}".format(d.left(),d.right(),d.right()-d.left()))
            #perwidth = d.right()-d.left()
            #KNOWN_WIDTH=1.5
            #inches = distance_to_camera(KNOWN_WIDTH, focalLength, perwidth)
            #cv2.putText(img, str(inches), (230, 50), 0, 0.8, (0, 255, 0), 2, cv2.LINE_AA)
            #print("inches {}".format(inches))
            #file=open('test.txt','w')
            #file.write(str(inches))
            #file.close()
            
            
            #c.send((str(inches)).encode())
            #c.close()
            #send_client()
        #else:
            #pass
            #c.send('9'.encode())
            #send_client_null()
    k = cv2.waitKey(30) & 0xff
    if k == 27: # press 'ESC' to quit
        break


cap.release()
cv2.destroyAllWindows()

